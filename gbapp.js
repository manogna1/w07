var path = require("path")
var express = require("express")
var logger = require("morgan")
var bodyParser = require("body-parser") // simplifies access to request body

var guest = express()  // make express app
var http = require('http').Server(guest)  // inject app into the server
guest.use(express.static(__dirname + '/assets'));
// ADD THESE COMMENTS AND IMPLEMENTATION HERE 
// 1 set up the view engine
guest.set("views", path.resolve(__dirname, "views")) // path to views
guest.set("view engine", "ejs") // specify our view engine

// 2 create an array to manage our entries app.locals is a built in object
var entries = []
guest.locals.entries = entries // now entries can be accessed in .ejs files

// 3 set up an http request logger to log every request automagically
guest.use(logger("dev"))     // app.use() establishes middleware functions
guest.use(bodyParser.urlencoded({ extended: false }))
guest.get("/", function (request, response) {
  response.render("page1")
})
guest.get("/page1", function (request, response) {
  response.render("page1")
})
guest.get('/index', function (req, res) {
res.render("index")
 })

guest.get('/page2', function (req, res) {
  res.render("page2")
 })
 guest.get('/page3', function (req, res) {
  res.render("page3")
 })
 guest.post("/contact", function (request, response) {
  var api_key = 'key-f880a7c13b2d757f0d9222969b4bb832';
  var domain = 'sandboxb548fda986db4ed594c1f98a2bbb949f.mailgun.org';
  var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
   
  var data = {
    from: 'Hello <postmaster@sandboxb548fda986db4ed594c1f98a2bbb949f.mailgun.org>',
    to: 'manogna.as28@gmail.com',
    subject: request.body.name,
    text: request.body.comment
  };
   
  mailgun.messages().send(data, function (error, body) {
    console.log(body);
    if(!error)
      response.send("Mail sent");
    else
    response.send("Mail unsent");
  });

 })

guest.get("/new-entry", function (request, response) {
  response.render("new-entry")
})
guest.post("/new-entry", function (request, response) {
  console.log(request.body);
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.")
    return
  }
  entries.push({  // store it
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  });
  response.redirect("/");
});
guest.use(function (request, response) {
  response.status(404).render("404")
})

// Listen for an application request on port 8081
http.listen(8081, function () {
  console.log('Guestbook app listening on http://127.0.0.1:8081/')
})
